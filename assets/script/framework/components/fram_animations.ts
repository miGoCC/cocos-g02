import { _decorator, Component, Node, Sprite, SpriteComponent, SpriteFrame } from 'cc';
const { ccclass, property } = _decorator;
/**
 * 播放帧动画组件 AUthor xxkk
*/
@ccclass('fram_animations')
export class fram_animations extends Component {

    private sprite: Sprite = null;
    @property(SpriteFrame)
    public spriteFrames: SpriteFrame[] = [];
    @property
    public duration: number = 0.2;
    public currentIndex = 0;
    private passTime = 0;

    public isPlaying = false;
    public isPause = false;
    public isStop = false;
    public isLoop = false;
    public isReversal = false;
    public isAutoPlay = false;
    public isAutoStop = false;
    public isAutoPause = false;
    public isAutoReversal = false;
    public isAutoLoop = false;
    public isAutoPlayOnce = false;

    show() {
        this.sprite = this.node.getComponent(Sprite);
        if (!this.sprite) {
            this.sprite = this.node.addComponent(Sprite);
        }
        this.isAutoPlay = this.spriteFrames.length > 1;
    }
    stop() {
        this.isStop = true;
        this.isPause = false;
        this.isPlaying = false;
        this.isReversal = false;
        this.isLoop = false;
        this.isAutoPlay = false;
        this.isAutoStop = false;
        this.isAutoPause = false;
        this.isAutoReversal = false;
        this.isAutoLoop = false;
        this.isAutoPlayOnce = false;
        this.passTime = 0;
    }

    update(deltaTime: number) {
        if (this.isAutoPlay) {
            this.passTime += deltaTime;
            if (this.passTime >= this.duration) {
                this.passTime = 0;
                this.sprite.spriteFrame = this.spriteFrames[this.currentIndex];
                this.currentIndex++;
                if (this.currentIndex >= this.spriteFrames.length) {
                    this.currentIndex = 0;
                }
            }
        }
    }
}

