import { _decorator, Button, CCInteger, Component, EventHandler, Node } from 'cc';
const { ccclass, property } = _decorator;
/** 
 * @author xxkk
 * @QQ 2959855822 小袖空空
 * @description  给Button组件加个长按触发事件 \\
 *  监听此组件所在节点触发长按事件
 *  便触发提交的EventHandler
 *  请拖动组件到引擎编辑器节点中使用即可，
 *  实际开发的目的是在Button组件下一起使用,来弥补button组件没有长按触发事件的不足
 * */
@ccclass('ButtonPlus')
export class ButtonPlus extends Button {

    @property({ type: CCInteger, tooltip: '长按触发长按的时长', displayName: '触发长按的时长(s)', step: 0.1 })
    public btn_hover_time: number = 1.0;// hover的时长,监听长按触发长按的总时间
    @property({ type: EventHandler, tooltip: "触发长按后的回调函数\\n当前节点有长按事件，则触发所有事件", displayName: '长按事件' })
    public longTouchEvents: EventHandler[] = [];//长按事件

    onEnable() {
        this.node.on(Node.EventType.TOUCH_START, this._onTouchStartPlus, this);
        this.node.on(Node.EventType.TOUCH_END, this._onTouchEndPlus, this);
        this.node.on(Node.EventType.TOUCH_CANCEL, this._onTouchCancelPlus, this);
    }
    onDisable() {
        this.node.off(Node.EventType.TOUCH_START, this._onTouchStartPlus, this);
        this.node.off(Node.EventType.TOUCH_END, this._onTouchEndPlus, this);
        this.node.off(Node.EventType.TOUCH_CANCEL, this._onTouchCancelPlus, this);
    }

    private _onTouchStartPlus() {
        // console.log("长按开始");
        this.scheduleOnce(this._trigger_events, this.btn_hover_time);
    }
    private _onTouchEndPlus() {
        // console.log("长按结束");
        this.unschedule(this._trigger_events)
    }
    private _onTouchCancelPlus() {
        // console.log("长按取消");
        this.unschedule(this._trigger_events)
    }

    private _trigger_events() {
        EventHandler.emitEvents(this.longTouchEvents)
    }
}

