// export enum LogLv {// 数值枚举不可重复
//   OFF = 0,
//   ERROR = 1,
//   WARN = 2,
//   INFO = 3,
//   DEBUG = 4,
//   TRACE = 5,
// }
// export default class Log {
//   private static outputFormat: string = '[{level}] {time}: ';
//   public static lv: Map<number, LogLv> = new Map();
//   static {
//     this.lv.set(LogLv.TRACE, LogLv.TRACE);
//     this.lv.set(LogLv.DEBUG, LogLv.DEBUG);
//     this.lv.set(LogLv.INFO, LogLv.INFO);
//     this.lv.set(LogLv.WARN, LogLv.WARN);
//     this.lv.set(LogLv.ERROR, LogLv.ERROR);
//     // 这里添加要打印的枚举
//   }

//   // public static setLevel(level: LogLv[]) {
//   //   Log.lv = level;
//   // }
//   public static setOutputFormat(format: string) {
//     this.outputFormat = format;
//   }
//   public static fm(lv: LogLv): string {//, ...args: any[]
//     return this.outputFormat.replace(/\{(\w+)\}/g, (_, key) => {
//       if (key === 'level') {
//         return LogLv[lv];
//       } else if (key === 'time') {
//         return new Date().toLocaleString();
//       }
//     }); //+ args.toString()
//   }
// }