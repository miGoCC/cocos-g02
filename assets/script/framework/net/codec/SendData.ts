let cmdIndexInc = 1;
export default class SendData {
    serviceAndMethod: string;
    message: any;// proto数据
    protoName: string;
    needEncrypt: boolean;
    encryptKey: string;
    sessionKey: string;
    cmdIndex: number;

    constructor(message, serviceAndMethod: string) {
        this.init(message, serviceAndMethod);
    }
    init(message, serviceAndMethod) {
        this.serviceAndMethod = serviceAndMethod;
        this.message = message;
        this.protoName = message._protoName;
        this.needEncrypt = false;
        this.encryptKey = null;
        this.sessionKey = null;
        this.cmdIndex = cmdIndexInc++;
    }
}