export default class LengthOptimizeConfig {
    encoderServiceNameMap = {};
    encoderProtoNameMap = {};
    decoderServiceNameMap = {};
    decoderProtoNameMap = {};
    constructor() {

    }
    addService(actName: string, replaceName: string) {
        this.encoderServiceNameMap[actName] = replaceName;
        this.decoderServiceNameMap[replaceName] = actName;
    }
    addProto(actName: string, replaceName: string) {
        this.encoderProtoNameMap[actName] = replaceName;
        this.decoderProtoNameMap[replaceName] = actName;
    }
}