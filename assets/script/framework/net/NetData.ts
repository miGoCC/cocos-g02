import BundleManager from "../manager/BundleManager";
// import { ResourceUtil } from "../utils/resourceUtil";
import NetConfig from "./NetConfig";
import { JsonAsset } from "cc";
// import protobuf from 'protobufjs';
/**
 * npm install protobufjs --save
 */
export default class NetData {
    static pbRoot: any = null;

    static init() {
        console.log("初始化NetData");
        return new Promise<void>((resolve) => {
            BundleManager.Instance.loadResType(NetConfig.protoInitPath, JsonAsset).then((content: JsonAsset) => {
                console.log(JSON.stringify(content.json)); 
                // NetData.pbRoot = protobuf.Root.fromJSON(content.json);
                console.log(NetData.pbRoot);
                resolve();
            });

        });



    }

    static createProtoInstance<T>(name: string): T {
        let messageCls = <any>this.pbRoot.lookupType(NetConfig.protoPackage + "." + name);
        // console.log(messageCls);
        let msg = messageCls.create();
        msg.protoCls = messageCls;
        msg._protoName = name;
        return msg;
    }

}