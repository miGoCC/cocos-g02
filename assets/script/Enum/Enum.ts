import { Enum } from "cc";

export enum LOADING {
    START = 'start'
}

export enum PREFAB_PATH {
    START_GAME = 'fight/startPanel', //开始游戏
    sk_ani_prefab = 'fight/sk',//共用的骨骼动画播放节点预制体,特殊骨骼动画预制体需要另外制作
    SLOT01_PANEL = 'fight/Slot01Panel',//机器01号主面板预制体
    LINE_NODE_PREFAB = 'commer/LineNode',//连线面板预制体
    MACHINE_LOADING_PANEL = 'commer/Slot01LoadingPanel',//游戏加载界面进度预制体
    SLOT02_PANEL = 'fight/Slot02Panel',//机器01号主面板预制体
}

export enum EventName {// 触发事件
    Spin_end = 'spingEnd',//机器spin停止事件
    Long_press = 'Long_press',//长按事件
}
export enum SlotState {// 游戏状态
    Normal = 'MAIN',//正常状态
    Free_1 = 'FREE',//slot02 3个狮子头触发 免费模式
    Free_2 = 'free2',//slot02 6个灯笼触发 免费模式
}

export class Constant {


}
