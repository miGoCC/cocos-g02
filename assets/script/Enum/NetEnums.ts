/**
 * In JS enum is an object of [value]: name
 * so you can get all values like that Object.keys(enum),
 * all names Object.values(enum)
 * and iterate in one go using for(const [value, name] of Object.entries(enum)) { ... }.
 * Beware that when you get values they will be strings,
 * not numbers as you would expect (since in JS keys of object are strings).
 */

export enum NetState {
  open = 0,
  close = 1,
}
export const enum NetEvent {
  onNetOpen = "onNetOpen",
  onNetError = "onNetError",
  onNetClose = "onNetClose",
  onNetTimeOut = "onNetTimeOut",
}
export enum NetManagerEvent {
  onConnecting = "onConnecting",
  onConnected = "onConnected",
  onDisconnected = "onDisconnected",
  onError = "onError",
  connectServer = "connectServer",// 客户端主动连接服务器事件
  onClose = "onClose",
}
export const enum SocketType {//游戏服务器连接类型
    logic = "logic",// 逻辑服务器游戏大厅
    chat = "chat", // 聊天服务器
}
export enum ReqError {
  WebsocketIsNull = "websocket is null",
  WebsocketIsNotOpen = "websocket is not open",
  WebsocketIsNotClosed = "websocket is not closed",
  WebsocketIsNotDestroyed = "websocket is not destroyed",
  WebsocketIsNotConnected = "websocket is not connected",
}
export enum NetManagerState {
  connecting = 0,
  connected = 1,
  disconnected = 2,
}
