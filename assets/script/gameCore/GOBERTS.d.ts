// 在 .d.ts 文件中定义接口类并导出

import { Node } from "cc";

export declare interface SpinVo {// 后端接口数据对象
    PayLines?:[number[]]
    Stops:[string[]]
    Type:string
    Win:number
    WinRule?:[{}]
}
export declare interface ResultVo {// 后端接口数据对象
    Bet:number;//下注
    Chips:number;//剩余筹码
    Free:number;//免费次数
    FreeWin:number;//免费赢的筹码
    Spins:SpinVo[];//旋转数据
    Win:number;//总赢
}
export declare interface LineConfig{
    configPos:number[];//线的位置
    color:string//线的颜色
    //其它属性
}
export declare interface LineNodeVo{
    lineConfigIndex:number;//配置中 奖励线 配置属性的索引 GameConfig.instance.reward_line_config
    nodes:Node[];//奖励节点
    lineNodes:Node[];//线的位置节点
    isSkAni:boolean;//是否是骨骼动画 如果是则直接播放动画而不闪烁节点
}

export declare interface _2Node{
    skNode:Node;//node1 存放骨骼动画节点
    icoNode:Node;//node2 存放图标节点
}

/**
 * 骨骼动画配置数据对象
 */
export declare interface SkConfig {
sk_name:string;//骨骼动画名称
sk_ani_name:string;//骨骼所需播放动画名称
ico_name:string;//骨骼动画对应滚动的图标名称
/** 骨骼动画缩放 x*/
sk_scaleX:number;
/**骨骼动画缩放 y*/
sk_scaleY:number;
/**
 * 骨骼动画prefab路径
 * 不同骨骼动画的预制体可能存在不相同而去重新指定
 */
prefab_path?:string;
sk_scaleZ?:number;//骨骼动画缩放
/**骨骼动画偏移 x*/
sk_offsetX:number;
/**骨骼动画偏移 y*/
sk_offsetY:number;
}

export declare interface RoleVo{// 暂时不用，直接用roleproto替代
     roleId:number;
     roleName?:string;// 角色昵称
     platformId:string;// 平台id
     areaId:number;// 角色所在区
     lastLoginTime:number;// 最后登陆时间（时间戳）
     consecutiveLoginDays:number;// 连续登陆天数（int）
     totalLoginDays:number;// 总登陆天数（int）
     lastRefreshLoginDaysTime:number;// 上次刷新连续登陆时间（时间戳）
}
export declare interface ServerVo{
     selectedServerId:number;
     areaVos:AreaVo[];
}
export declare interface AreaVo{
     areaId:number;
     roleId?:number;
}
    export declare interface Material {
    name: string;
    type: string;
    price: number;
    quantity: number;
   }
   
   export declare interface ConcreteMaterial extends Material {
    color: string;
    length: number;
   }
   
   export declare interface AnotherMaterial extends Material {
    width: number;
    height: number;
   }
//    // 使用 ConcreteMaterial 类型定义变量
//    let concreteMaterial: ConcreteMaterial = {
//     name: '石头',
//     type: '基础材料',
//     price: 10,
//     quantity: 1000,
//     color: '白色',
//     length: 100,
//    };
   
//    // 使用 AnotherMaterial 类型定义变量
//    let anotherMaterial: AnotherMaterial = {
//     name: '纸张',
//     type: '基础材料',
//     price: 2,
//     quantity: 1000,
//     width: 20,
//     height: 30,
//    };