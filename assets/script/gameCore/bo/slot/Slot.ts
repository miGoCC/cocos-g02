import { SlotState } from "../../../Enum/Enum";
import { SlotPanel } from "../../../uiComponent/slot/SlotPanel";
import { ResultVo } from "../../GOBERTS";

export class Slot {

    static MaxBet: number = 1000;//最大押注
    static MinBet: number = 100;//最小押注
    static BaseBet: number = 50;//基础倍率

    protected coin1: number;//金币1 游戏币
    protected coin2: number;//金币2 代币
    protected myBet: number;//押注

    public jackpot: Map<string, number> = new Map();//大小奖数据 奖池 TODO 待完善
    public panel: SlotPanel;// 数据对象与Ui面板互调

    protected win: number;// 赢取金额 单局刷新
    protected stopIcon: [string[]] = null;// 单局游戏停止后收到后端的结果图片名称矩阵 每局游戏刷新
    public PayLines: [number[]] = null;// 单局游戏停止后收到后端的结果线段矩阵 每局游戏刷新

    public result: ResultVo = null;// 结果数据

    constructor(panel: SlotPanel) {
        this.panel = panel;
    }
    init() {// 将数据赋予面板ui显示
        this.Coin1 = 10000
        this.Coin2 = 1000
        this.Win = 0
        this.MyBet = 100
        this.StopIcon = null
    }
    clean() {// 单局游戏开始前调用 清理一次
        this.stopIcon = null
        this.Win = 0
        // this.result = null
        this.PayLines = null
    }
    Result(val: any) {// 网络请求得到数据 单局结果赋值 收到的是json
        const res: ResultVo = JSON.parse(val);
        // console.log("res", res);
        this.result = res
        this.StopIcon = res.Spins[0].Stops
        this.PayLines = res.Spins[0].PayLines
    }
    // 更新请求到的数据到UI面板上 为了实现动态展示
    updateSelfRequestDataForUI() {
        this.Coin1 = this.result.Chips
        this.Win = this.result.Win
    }
    public set Coin1(value: number) {
        this.coin1 = value;
        this.panel.tweenLableFun(1)
    }

    public set Coin2(value: number) {
        this.coin2 = value;
        this.panel.tweenLableFun(2)
    }

    public set MyBet(value: number) {
        this.myBet = value;
        this.panel.tweenLableFun(3)
    }

    public get MyBet(): number {
        return this.myBet;
    }

    public set Win(value: number) {
        this.win = value;
        this.panel.tweenLableFun(4)
    }
    public get Win(): number {
        return this.win;
    }
    public get Coin1(): number {
        return this.coin1;
    }
    public get Coin2(): number {
        return this.coin2;
    }

    public get StopIcon(): [string[]] {
        return this.stopIcon;
    }
    public set StopIcon(value: [string[]]) {
        // console.log("StopIcon", value);
        this.stopIcon = value;
    }
}