import { SlotState } from "../../../Enum/Enum";
import { Util } from "../../../framework/utils/util";
import { Slot02Panel } from "../../../uiComponent/slot/Slot02Panel";
import { ResultVo, SpinVo } from "../../GOBERTS";
import { Slot } from "./Slot";


export class Slot02 extends Slot {

    protected slotState: SlotState = SlotState.Normal;//游戏状态
    public _canBet: boolean = true// 是否允许押注
    public panel: Slot02Panel;
    private _spin: SpinVo;

    constructor(panel: Slot02Panel) {
        super(panel);
        this.panel = panel;
    }

    init(): void {
        super.init();
        this.slotState = SlotState.Normal;
    }
    //游戏开始时调用 网络请求得到数据 单局结果赋值 收到的是json
    Result(val: any) {
        let res: ResultVo = JSON.parse(val);
        // console.log("res", res);
        this.result = res
        this.nextResult()
    }

    public get HasNextSpin(): boolean {// 是否有下一次旋转
        return this.result && this.result.Spins.length > 0
    }

    public get NextState() { // 获取下一次旋转的状态
        return this.result.Spins.length > 0 ? this.result.Spins[0].Type : SlotState.Normal;
    }

    nextResult() {// 如果存在后续免费次数，则刷新下一次滚动结果
        const spin: SpinVo = this.result.Spins[0]
        this.StopIcon = spin.Stops
        this.PayLines = spin.PayLines
        this._spin = spin;
        this.slotState = SlotState[Util.getKey(SlotState, spin.Type)]// 刷新当前游戏状态

        this.result.Spins.splice(0, 1)// 删除已经刷新的结果
        this.panel._playFreeAnimation = this.slotState == SlotState.Normal && this.NextState != SlotState.Normal // 判断接下来是否播放免费模式过渡动画
    }

    //游戏结束 更新请求到的数据到UI面板上 为了实现动态展示
    updateSelfRequestDataForUI() {
        this.Win = this._spin.Win
        this.Coin1 = this.Coin1 + this.Win
    }

    public set SlotState(value: SlotState) {
        this.slotState = value;
    }
    public get SlotState(): SlotState {
        return this.slotState;
    }

    public set MyBet(value: number) {// 添加押注管控，免费模式不再可押注
        if (this._canBet) {
            this.myBet = value;
            this.panel.tweenLableFun(3)
        }
    }

    public get MyBet(): number {
        return this.myBet;
    }

}