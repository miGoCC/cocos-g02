import { PREFAB_PATH } from "../Enum/Enum";
import Singleton from "../framework/bo/Singleton";
import { LineConfig, SkConfig } from "./GOBERTS";

export default class GameConfig extends Singleton {

    public static get instance(): GameConfig {
        return super.GetInstance<GameConfig>();
    }

    public static readonly fram_rate: number = 60;// 游戏配置帧率 暂不支持修改

    // 配置骨骼动画信息与图标对应名称
    public readonly animation_config: Map<string, SkConfig> = new Map<string, SkConfig>();
    // 机器奖励线配置
    public reward_line_config: LineConfig[] = [];
    //奖励线图标轴位置 最后一个为标头轴位置
    public readonly reward_line_y: number[] = [137, 0, -137, 67];
    public readonly reward_line_x: number[] = [-457, -270.2, -90, 90, 270.2, 457, -475];
    //后端图片名称与前端图片调用序列号 抹平差异
    public readonly img_name: Map<string, number> = new Map<string, number>();

    /**
     * 动态初始化机器动画资源配置数据
     * 骨骼动画配置数据，正式开发可配置在excel表格中，但实际开发直接配置在代码中比较方便调试
     * 配置原因：骨骼动画的播放尺寸大小和位置总是对不上，需要稍微修改
     * @param prefix 机器id 作为机器动画配置信息key的前缀
     */
    initMachine(prefix: string) {
        switch (prefix) {
            case "01":// 机器编号01
                this.animation_config.set(prefix + "H2_1", {
                    sk_ani_name: "H2",
                    sk_name: "H2",
                    sk_scaleX: 1.14,
                    sk_scaleY: 1,
                    sk_offsetX: -1,
                    sk_offsetY: 0,
                    // prefab_path: PREFAB_PATH.sk_ani_prefab,
                    ico_name: 'H2_1',
                })
                    .set(prefix + "H1_1", {
                        ico_name: 'H1_1',
                        sk_ani_name: "H1",
                        sk_name: "H1",
                        sk_scaleX: 1.065,
                        sk_scaleY: 0.96,
                        sk_offsetX: 2,
                        sk_offsetY: 0,
                        // prefab_path: PREFAB_PATH.sk_ani_prefab,
                    })
                    .set(prefix + "H3_1", {
                        ico_name: 'H3_1',
                        sk_ani_name: "H3",
                        sk_name: "H3",
                        sk_scaleX: 1.17,
                        sk_scaleY: 0.96,
                        sk_offsetX: 0,
                        sk_offsetY: 2,
                    })
                    .set(prefix + "H4_1", {
                        ico_name: 'H4_1',
                        sk_ani_name: "H4",
                        sk_name: "H4",
                        sk_scaleX: 1.14,
                        sk_scaleY: 1.04,
                        sk_offsetX: 0,
                        sk_offsetY: 2,
                    })
                    .set(prefix + "Scatter_1", {
                        ico_name: 'Scatter_1',
                        sk_ani_name: "Scatter",
                        sk_name: "Scatter",
                        sk_scaleX: 0.77,
                        sk_scaleY: 0.63,
                        sk_offsetX: 0,
                        sk_offsetY: 0,
                    })

                this.reward_line_config = this.reward_line_config.concat(// 动态初始化加载奖励线配置
                    { configPos: [1, 1, 1, 1, 1], color: '#9E29EC' },
                    { configPos: [0, 0, 0, 0, 0], color: '#4C34C9' },
                    { configPos: [2, 2, 2, 2, 2], color: '#4C34C9' },
                    { configPos: [0, 1, 2, 1, 0], color: '#D3A71B' },
                    { configPos: [2, 1, 0, 1, 2], color: '#23DFF8' },
                    { configPos: [1, 0, 0, 0, 1], color: '#2378F8' },
                    { configPos: [1, 2, 2, 2, 1], color: '#E01FBA' },
                    { configPos: [0, 0, 1, 2, 2], color: '#E01FBA' },
                    { configPos: [2, 2, 1, 0, 0], color: '#F12B63' },
                    { configPos: [1, 0, 1, 2, 1], color: '#FA6823' },
                    { configPos: [1, 2, 1, 0, 1], color: '#2378F8' },
                    { configPos: [0, 1, 1, 1, 0], color: '#23DFF8' },
                    { configPos: [2, 1, 1, 1, 2], color: '#9E29EC' },
                    { configPos: [0, 1, 0, 1, 0], color: '#F12B63' },
                    { configPos: [2, 1, 2, 1, 2], color: '#FA6823' },
                    { configPos: [1, 1, 0, 1, 1], color: '#27E727' },
                    { configPos: [1, 1, 2, 1, 1], color: '#F03232' },
                    { configPos: [0, 0, 2, 0, 0], color: '#FA6823' },
                    { configPos: [2, 2, 0, 2, 2], color: '#D3A71B' },
                    { configPos: [0, 2, 2, 2, 0], color: '#27E727' },
                    // { linePos: [] },
                )
                this.img_name.set("Sym1", 0);
                this.img_name.set("Sym2", 1);
                this.img_name.set("Sym3", 2);
                this.img_name.set("Sym4", 3);
                this.img_name.set("Sym5", 5);
                this.img_name.set("Sym6", 6);
                this.img_name.set("Sym7", 7);
                this.img_name.set("Sym8", 8);
                this.img_name.set("Sym9", 9);
                this.img_name.set("Scatter", 4);
                this.img_name.set("Wild", 10);
                this.img_name.set("Sym10", 11);
                break;
            default:
                console.log("没有找到机器编号" + prefix);// 数据配置在excel中会出现这种情况
                break;
        }
    }


}