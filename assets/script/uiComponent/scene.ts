import { _decorator, Component, Node } from 'cc';
import { UIManager } from '../framework/manager/uiManager';
import BundleManager from '../framework/manager/BundleManager';
import { PREFAB_PATH } from '../Enum/Enum';
import { BundleType } from '../Enum/FramworkEnums';
const { ccclass, property } = _decorator;

@ccclass('scene')
export class scene extends Component {
    async start() {
        console.log("游戏启动");
        // 初始化游戏
        await BundleManager.Instance.initRescources();
        // ...
        UIManager.instance.showDialog(PREFAB_PATH.MACHINE_LOADING_PANEL, BundleType.resourcesBundle, [PREFAB_PATH.SLOT02_PANEL]);
    }

    update(deltaTime: number) {

    }
}

