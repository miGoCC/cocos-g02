import { _decorator, Tween } from 'cc';

import { _2Node, LineNodeVo } from '../../gameCore/GOBERTS';
import { SlotPanel } from './SlotPanel';
import { EventName, SlotState } from '../../Enum/Enum';
import { ClientEvent } from '../../framework/bo/ClientEvent';
import GameConfig from '../../gameCore/GameConfig';
import { Slot02 } from '../../gameCore/bo/slot/Slot02';
import { Util } from '../../framework/utils/util';
import { PoolManager } from '../../framework/manager/poolManager';
const { ccclass } = _decorator;

@ccclass('Slot02Panel')
export class Slot02Panel extends SlotPanel {

    protected slotVo: Slot02 = null;//继承 Slot
    public _playFreeAnimation: boolean = false;// 是否播放免费动画

    show() {
        console.log(this.node.name + " show");
        ClientEvent.on(EventName.Spin_end, this.onSpinEnd, this)
        GameConfig.instance.initMachine(this._machineId)
        this.btnChangeFast()
        this.slotVo = new Slot02(this);
        this.slotVo.init();
    }

    public playAnimation(): void {

        if (this.slotVo.SlotState == SlotState.Normal
            || this.slotVo.SlotState == SlotState.Free_1) {//正常游戏模式播放动画
            super.playAnimation();
            if (this.slotVo.SlotState == SlotState.Normal) {
                // 正常模式结束位置
                this.gameEndReflashUIState();
                this.slotVo._canBet = true;// 押注按钮可用 游戏结束
                // 正常模式结束位置
            }
        }
    
        
        if (this._playFreeAnimation) {// 判断下一次是否过渡到免费模式
            this._playFreeAnimation = false;
            // 播放过度动画
            console.log("播放免费模式过渡动画");// TODO
            setTimeout(() => {
                console.log("播放免费模式过渡动画结束");
                console.log("开始等待用户选择一种免费模式");  
                // 选择完成 设置数据状态 然后退出选择界面
                this._canSpin = true//解除禁用 spin按钮 允许用户开始游戏
                //设置自动旋转定时任务 退出免费模式时需要删除 TODO
            }, 5000)
        }
    }
    // 游戏结束刷新或者重置UI界面
    gameEndReflashUIState() {
        if (!this.slotVo.HasNextSpin) //重置一次金币，防止异常
            this.slotVo.Coin1 = this.slotVo.result.Chips;

        this.skPanelNode.children.forEach(element => {// 骨骼动画节点有时没清理掉 这里打个补丁
            PoolManager.instance.putNode(element)
        })
    }
    // public getNodeArrByBack(): LineNodeVo[] {
    //     if (this.slotVo.SlotState == SlotState.Normal) {//正常游戏模式过滤出得分节点
    //         return super.getNodeArrByBack();
    //     }
    //     return super.getNodeArrByBack();
    // }

    flashResultData() {// 开始滚动 刷新滚动结果
        this.slotVo._canBet = false // 进入不可押注状态
        if (this.slotVo.HasNextSpin) {
            this.slotVo.nextResult();  //进入免费模式，则刷新后续获得的图标
            console.log("免费模式", this.slotVo.SlotState, this.slotVo.result.Spins.length);

        } else { //普通模式下，发起网络请求，否则刷新下一轮结果
            this.slotVo.Coin1 -= this.slotVo.MyBet//扣除押注金币
            Util.httpGET('http://192.168.31.203:3000/api_t1/spin', (val) => {  // 网络请求 
                this.slotVo.Result(val);// 设置结果

                if (this._playFreeAnimation)
                    this._canSpin = false//进入免费模式播放选择动画时 不可滚动

            }, { wager: this.slotVo.MyBet / 50 + '' })//wager是bet（基础倍率50）接口请求所需
        }

    }
    //==================私有配置变量 不支持随意修改================

    /**依据游戏规则整理需要播放动画的节点 tips:配置规则 连线
     * @deprecated 采用后端奖励计算结果 所以弃用
    */
    // protected getNodeArr(): LineNodeVo[] {
    //     // let nodeArr: Set<Node> = new Set<Node>();
    //     let lineNode_temp: LineNodeVo[] = [];//奖励线节点数组
    //     const lineConfig: LineConfig[] = GameConfig.instance.reward_line_config;
    //     for (let i = 0; i < lineConfig.length; i++) {
    //         const line: LineConfig = lineConfig[i]
    //         let lineNode: LineNodeVo = { nodes: [], lineNodes: [], isSkAni: false, lineConfigIndex: i };

    //         for (let j = 0; j < this.reelNodeArr.length; j++) {
    //             let nd: Node = this.reelNodeArr[j]
    //             let r2: Node = nd.getChildByName("r2")
    //             // r2.children.forEach((temp) => { nodeArr.add(temp) })
    //             const index = line.configPos[j]
    //             const node: Node = r2.children[index]

    //             if (j == 0) {// TODO 游戏规则待完善
    //                 lineNode.nodes.push(node)
    //                 const ico_name = this.getSpriteName(node)
    //                 const config: SkConfig = GameConfig.instance.animation_config.get(this._machineId + ico_name)
    //                 lineNode.isSkAni = config ? true : false;
    //             }
    //             else if (j >= 1) {
    //                 if (this.getSpriteName(node) == this.getSpriteName(lineNode.nodes[0])) {
    //                     lineNode.nodes.push(node)
    //                 }
    //                 else {
    //                     break;
    //                 }
    //                 console.log(i + 1, "line", lineNode.nodes[0].name, j + 1, "列", index + 1, "行", "图片名称", this.getSpriteName(node));
    //             }
    //         }
    //         lineNode_temp.push(lineNode)
    //     }
    //     let nodeArr: LineNodeVo[] = [];
    //     for (let i = 0; i < lineNode_temp.length; i++) {// 得分节点过滤
    //         if (lineNode_temp[i].nodes.length > 1) {
    //             nodeArr.push(lineNode_temp[i])
    //             // this.inidRewardLine(lineNode_temp[i]);
    //         }
    //     }
    //     console.log("得分线条数", nodeArr.length);
    //     return nodeArr;
    // }

    // ==============================重载继承方法 实现多玩法==============================


}

