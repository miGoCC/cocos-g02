import { _decorator, Component, Node, Prefab, ProgressBar } from 'cc';
import BundleManager from '../../framework/manager/BundleManager';
import { BundleType } from '../../Enum/FramworkEnums';
import { UIManager } from '../../framework/manager/uiManager';
import { PREFAB_PATH } from '../../Enum/Enum';
const { ccclass, property } = _decorator;

@ccclass('Slot01LoadingPanel')
export class Slot01LoadingPanel extends Component {// 正在加载 界面 显示进度条
    @property({ type: ProgressBar, displayName: '进度条' })
    private progressBar: ProgressBar = null!;

    show(preLoadPanelName: string) {
        console.log('Slot01LoadingPanel show ');
        this.progressBar.progress = 0
        BundleManager.Instance.preloadPrefabAsync(preLoadPanelName, BundleType.resourcesBundle, (completedCount: number, totalCount: number, item) => {
            //console.log("progress:", completedCount, totalCount);
            this.progressBar.progress = completedCount / totalCount     // 加载进度改变
        },
            (err: Error | null, data: any) => {// 资源预加载结束
                if (err) {
                    return console.error("preload failed: " + err.message);
                }
                // console.log("preload success: " + data);
                setTimeout(() => {
                    UIManager.instance.showDialog(preLoadPanelName);
                    UIManager.instance.hideDialog(PREFAB_PATH.MACHINE_LOADING_PANEL);// 释放加载界面
                }, 150);
            }

        );
    }

}

